﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class UnitLair : MonoBehaviour {
	public GameObject unit;
	public GameObject[] wayponits = new GameObject[2];
	private Vector3 curWaypoint;
//	List<string> actionNames = new List<string>{"Idle", "Walk", "Wait", "Attack", "Cast", "Die"};
	//action curAction;
	//Dictionary<action, Animation> animDict = new Dictionary<action, Animation>;
	Animation animation;
	private bool moving;
	float turnSpeed = 10;
	float moveSpeed = 1	;

    
	void Awake(){
		unit = Instantiate(unit, new Vector3(0f, 0f, 3f), Quaternion.identity) as GameObject;
		animation = unit.GetComponent<Animation>();

	}

	// Use this for initialization
	void Start () {
		curWaypoint = wayponits[0].transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (!moving){
			return;
		}
		animation.Play("Walk");
		for (int i = 0; i < 2; i++){
		    if (unit.transform.position == wayponits[i].transform.position){
			    curWaypoint = wayponits[Mathf.Abs(1 - i)].transform.position;
		    }
		}
		float step = moveSpeed * Time.deltaTime;
		unit.transform.position = Vector3.MoveTowards (unit.transform.position, curWaypoint, step);

		step = turnSpeed * Time.deltaTime;
		Vector3 targetDir = curWaypoint - unit.transform.position;
		Vector3 newDir = Vector3.RotateTowards (unit.transform.forward, targetDir, step, 0.0F);
		unit.transform.rotation = Quaternion.LookRotation (newDir);

	}

	public void PlayAnimation(string animationName){
	    try{
			animation.Play(animationName);
		}
    	catch{
			Debug.LogError(animationName + " animation name not found on prefab.");
		}
	}

	public void ToggleUnitMove(){
		moving = !moving;
	}
}
