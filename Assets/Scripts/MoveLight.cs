﻿using UnityEngine;
using System.Collections;

public class MoveLight : MonoBehaviour {
	private GameObject thisCamera;
	private bool turning;
	// Use this for initialization
	void Awake () {
	
	}
	void Update () {
		if (turning){
			this.transform.RotateAround(Vector3.zero, Vector3.up, 40 * Time.deltaTime);
		}
		
	}
	
	public void Turn(bool swch){
		turning = !turning;
	}
}
