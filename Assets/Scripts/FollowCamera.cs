﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {
	private bool turning;
	private GameObject target;
	// Use this for initialization
	void Start () {
		target = GameObject.Find("TestObject").GetComponent<UnitLair>().unit;	
	}
	
	
	// Update is called once per frame
	void Update () {
		if (turning){
			Camera.main.transform.RotateAround(target.transform.position, Vector3.up, 1f);
		}
		Camera.main.transform.LookAt(target.transform.position);
		
	}
	
	public void Turn(bool swch){
		turning = !turning;
	}
}
